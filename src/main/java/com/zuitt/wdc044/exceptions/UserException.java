package com.zuitt.wdc044.exceptions;

public class UserException extends Exception {

    public UserException (String message){
        //with the help of super method, it will inherit parent's class properties and methods.
        //we will have access to its constructors
        super(message);
    }

}